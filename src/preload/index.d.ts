import { ElectronAPI } from '@electron-toolkit/preload'

declare global {
  interface Window {
    electron: ElectronAPI
    api: {
      setWallpaper: (wallpaperUrl: string) => void
      downloadWallpaper: (wallpaperUrl: string) => Promise<Buffer>
    }
  }
}
