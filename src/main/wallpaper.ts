import { IpcMainEvent, IpcMainInvokeEvent, ipcMain } from 'electron'
import path from 'node:path'
import fs from 'node:fs'
import download from 'download'
import { setWallpaper } from 'wallpaper'

/**
 * Create temp folder if not exist
 */
const temp = path.join(process.resourcesPath, 'temp')
if (!fs.existsSync(temp)) {
  fs.mkdirSync(temp)
}

ipcMain.on('setWallpaper', async (_event: IpcMainEvent, wallpaperUrl: string): Promise<void> => {
  console.log('main ----', wallpaperUrl)
  const options = {
    url: wallpaperUrl,
    dest: process.resourcesPath
  }
  console.log('main ----', options)
})

ipcMain.handle(
  'downloadWallpaper',
  (_event: IpcMainInvokeEvent, wallpaperUrl: string): Promise<Buffer> => {
    return download(wallpaperUrl, temp, { extract: true, filename: 'wallpaper.jpg' }).then(
      (res) => {
        console.log('main ----', res)
        return res
      }
    )
  }
)
